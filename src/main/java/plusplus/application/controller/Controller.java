/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plusplus.application.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import plusplus.application.model.Package;

/**
 *
 * @author Korisnik
 */
public class Controller {

    private static Controller instance;
    private final Gson gson;
    private List<Package> packages;

    private Controller() throws IOException {
        gson = new GsonBuilder().setPrettyPrinting().create();

    }

    public static Controller getInstance() throws IOException {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    public List<Package> getPackages() {
        return packages;
    }

    public void addPackage(Package pack) {
        if (packages == null) {
            packages = new ArrayList<>();
        }
        packages.add(pack);
    }

    public void readPackages() throws FileNotFoundException {

        List<Package> packages = gson.fromJson(new JsonReader(new FileReader("file.json")), new TypeToken<List<Package>>() {
        }.getType());

        if (packages == null) {
            packages = new ArrayList<>();
        }
        System.out.println("PAKETI");

        for (Package aPackage : packages) {
            if (aPackage.getPackageExpiration().before(new Date())) {
                System.out.println("Package with id : " + aPackage.getPersonalId() + " has expired");
            } else {
                System.out.println("Package with id : " + aPackage.getPersonalId() + " is waitng to be sent");
            }
        }
    }

    public void writePackagesInJSONFIle(Package pack) throws IOException {

        System.out.println("WRITING PACKAGE IN FILE");
        packages = gson.fromJson(new JsonReader(new FileReader("file.json")), new TypeToken<List<Package>>() {
        }.getType());

        if (packages == null) {
            packages = new ArrayList<>();
        }

        packages.add(pack);
        try (Writer writer = new FileWriter("file.json")) {
            gson.toJson(packages, writer);
        }

    }

}
