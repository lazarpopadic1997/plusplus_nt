/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plusplus.application.communication;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import plusplus.application.controller.Controller;
import plusplus.application.model.Package;

/**
 *
 * @author Korisnik
 */
public class Communicate {

    Socket socket;
    private static Communicate instance;

    public Communicate() throws Exception {
        socket = new Socket("hermes.plusplus.rs", 4000);
    }

    public static Communicate vratiInstancu() throws Exception {
        if (instance == null) {
            instance = new Communicate();
        }
        return instance;
    }

    public synchronized void receive() throws Exception {
        try {

            DataInputStream in = new DataInputStream(socket.getInputStream());
            byte bajt = in.readByte();
            int capacity = 0;
            if (bajt == 1) {
                capacity = 16;
            } else {
                capacity = 12;
            }
            byte[] bajtovi = in.readNBytes(capacity - 1);
            ByteBuffer buffer = ByteBuffer.allocate(capacity);
            buffer.put(bajt);
            for (byte b : bajtovi) {
                buffer.put(b);
            }
            System.out.println("BUFFER ALLOCATED  : " + buffer.toString());

            Package pack = new Package(buffer);
            Calendar calendar = Calendar.getInstance();
            pack.setPackageReceived(calendar.getTime());
            calendar.add(Calendar.SECOND, (int) pack.getDelay());
            pack.setPackageExpiration(calendar.getTime());
            System.out.println("PACKAGE : " + pack);

            Controller.getInstance().writePackagesInJSONFIle(pack);

            System.out.println("=====================================");
        } catch (IOException ex) {
            throw new Exception("Error accepting object " + ex.getMessage());
        }
    }

    public synchronized void send(Package pack) throws Exception {
        try {
            ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
            out.writeObject(pack);
            out.flush();
            System.out.println("=============================================================");
            System.out.println("PACKAGE WITH ID : " + pack.getPersonalId() + " HAS BEEN SENT, TIME :  " + new Date());
            System.out.println("=============================================================");

        } catch (Exception ex) {
            throw new Exception("An error occurred while sending an object! " + ex.getMessage());
        }
    }

}
