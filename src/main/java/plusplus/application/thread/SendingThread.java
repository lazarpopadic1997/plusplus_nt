/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plusplus.application.thread;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import plusplus.application.communication.Communicate;
import plusplus.application.controller.Controller;
import plusplus.application.model.Package;

/**
 *
 * @author Korisnik
 */
public class SendingThread extends Thread {

    Gson gson;

    public SendingThread() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public void run() {

        try {
            Communicate comm = Communicate.vratiInstancu();
            while (true) {
                List<Package> packages = gson.fromJson(new JsonReader(new FileReader("file.json")), new TypeToken<List<Package>>() {
                }.getType());

                synchronized (this) {

                    if (packages == null) {
                        packages = new ArrayList<>();
                    }
                    for (Package aPackage : packages) {
                        this.wait(aPackage.getDelay() * 1000);

                        comm.send(aPackage);
                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(SendingThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
