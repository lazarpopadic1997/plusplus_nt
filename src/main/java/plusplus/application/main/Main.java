/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plusplus.application.main;

import java.util.logging.Level;
import java.util.logging.Logger;
import plusplus.application.thread.Client;
import plusplus.application.communication.Communicate;
import plusplus.application.controller.Controller;
import plusplus.application.thread.SendingThread;

/**
 *
 * @author Korisnik
 */
public class Main {

    public static void main(String[] args) {
        try {
            SendingThread st = new SendingThread();
            st.start();
            Controller.getInstance().readPackages();

            Communicate.vratiInstancu();

            Client client = new Client();
            client.start();

        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
