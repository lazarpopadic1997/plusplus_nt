/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package plusplus.application.model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author Korisnik
 */
public class Package implements Serializable {

//    @SerializedName("idPackage")
    private int idPackage;
//    @SerializedName("length")
    private int length;
//    @SerializedName("personalId")
    private int personalId;
//    @SerializedName("delay")
    private int delay;
    private Date packageReceived;
    private Date packageExpiration;

    public Package() {
    }

    public Package(int idPackage, int length, int personalId, int delay, Date packageReceived, Date packageExpiration) {
        this.idPackage = idPackage;
        this.length = length;
        this.personalId = personalId;
        this.delay = delay;
        this.packageReceived = packageReceived;
        this.packageExpiration = packageExpiration;
    }

    public Package(ByteBuffer buffer) {
        this.idPackage = buffer.get(0);
        this.length = buffer.get(4);
        this.personalId = buffer.get(8);
        this.delay = buffer.get(12);
    }

    public int getIdPackage() {
        return idPackage;
    }

    public void setIdPackage(int idPackage) {
        this.idPackage = idPackage;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getPersonalId() {
        return personalId;
    }

    public void setPersonalId(int personalId) {
        this.personalId = personalId;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public Date getPackageExpiration() {
        return packageExpiration;
    }

    public Date getPackageReceived() {
        return packageReceived;
    }

    public void setPackageExpiration(Date packageExpiration) {
        this.packageExpiration = packageExpiration;
    }

    public void setPackageReceived(Date packageReceived) {
        this.packageReceived = packageReceived;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Package other = (Package) obj;
        if (this.personalId != other.personalId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Package{" + "idPackage=" + idPackage + ", length=" + length + ", personalId=" + personalId + ", delay=" + delay + ", packageReceived=" + packageReceived + ", packageExpiration=" + packageExpiration + '}';
    }

}
